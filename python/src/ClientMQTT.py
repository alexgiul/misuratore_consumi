'''
Created on 03 mag 2017

@author: GIULI370

Sintassi Crontab
    minuto
    ora
    giorno del mese
    mese
    giorno della settimana

Inserire in crontab:

    */15 8-21 * * * /usr/bin/updatedb

Il valore dei minuti e' stato impostato come */10; cio' significa che 
la nostra operazione sara' eseguita ogni 10 minuti dalle 8 alle 20.

'''
import paho.mqtt.client as mqtt

import ssl
import json

import sqlite3

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, obj, flags, rc):
    print("Connected with result code "+str(rc))
    if rc==0:
        
        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        # Subscribe to the AWS thing's delta-topic in order to receive state change
        client.subscribe("$aws/things/power_meter_rpi/shadow/update/delta", qos=0)
        # Publish a state to another topic for state reports
        payload= "{\"state\":{\"reported\":{\"device\":\"connected\"}}}"
        client.publish("$aws/things/power_meter_rpi/shadow/update", payload, 0, True)
        
        

# The callback for when a PUBLISH message is received from the server.
def on_message(client, obj, msg):
    print("Message Received : " + msg.topic+" Data Received : "+str(msg.payload))
    
    payload = str(msg.payload).replace("b'", "", 1).replace("'", "")
    payloadJson = json.loads(payload)
    

def setup_database(con):
    try:
        cursor = con.cursor()
        
        #create 'measure' table
        #cursor.execute('''DROP TABLE measure''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS measure (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, device TEXT, value INTEGER, processed CHAR(1) DEFAULT 'N', timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)''')

        #create 'events' table
        #cursor.execute('''DROP TABLE events''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, device TEXT, event TEST, level VARCHAR(10), timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)''')
        
        #create 'crews' table
        #cursor.execute('''DROP TABLE crews''')
        #cursor.execute('''CREATE TABLE IF NOT EXISTS crews(id INTEGER, first_name TEXT, other_names TEXT, role TEXT, stage_name TEXT, biography TEXT, picture TEXT)''')
        
        #create 'configuration' table
        #cursor.execute('''DROP TABLE configuration''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS configuration(key TEXT, value TEXT)''')
    
    except Exception as e:
        raise e

if __name__ == '__main__':

    print("Open database\r\n")
    try:        
        connection = sqlite3.connect("../power_meter.db")
        setup_database(connection)
    except Exception as e:        
        print(str(e))
        quit()

    
    
    awshost = "a2xvqvuxqta1yx.iot.us-west-2.amazonaws.com"
    awsport = 8883
    clientId = "myThingName"
    thingName = "myThingName"
    
    caPath = "../awsiot-cert/root-CA.crt"
    certPath = "../awsiot-cert/power_meter_rpi.cert.pem"
    keyPath = "../awsiot-cert/power_meter_rpi.private.key"

    mqtt_client = mqtt.Client(client_id="power_meter_rpi", clean_session=True)
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message
    #mqtt_client.on_log = on_log
    
    mqtt_client.tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
    '''
    mqtt_client.tls_set("./ca-cert.pem", certfile="./xxx.pem.crt",
                        keyfile="./xxxx.pem.key",
                        tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
    '''
    
    mqtt_client.connect(awshost, awsport, keepalive=60)
    
    
    
    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.
    mqtt_client.loop_forever()
    
    #mqtt_client.loop_start()