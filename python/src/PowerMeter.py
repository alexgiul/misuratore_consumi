'''
Created on 22/set/2016

@author: GIULI370

Sintassi Crontab
    minuto
    ora
    giorno del mese
    mese
    giorno della settimana

Inserire in crontab:

    */10 8-20 * * * /usr/bin/updatedb

Il valore dei minuti e' stato impostato come */10; cio' significa che 
la nostra operazione sara' eseguita ogni 10 minuti dalle 8 alle 20.

'''
import time
import serial   # https://pythonhosted.org/pyserial/shortintro.html
#from PyCRC.CRCCCITT import CRCCCITT
from CRCCCITT import CRCCCITT
import sys 

import sqlite3


# sudo apt-get install python-serial

def try_io(call, tries=10):
    
    assert tries > 0
    
    result = None

    while tries:
        try:
            result = call()
            time.sleep(0.005)
        except IOError as e:
            tries -= 1
        else:
            break

    if not tries:
        raise IOError('Unable to send UART command over interface\n')

    return result

def write_uart_line(ser, cmd, timeout=20, output=None):

    assert ser is not None

    cmd_str= cmd + "\n"
    
    ser.write(cmd_str.encode('ascii') )

    tic  = time.time()

    # you can use if not ('\n' in buff) too if you don't like re
    while ( (time.time() - tic) < timeout ):
        line = (ser.readline()).decode("utf-8")
        
        print (line.strip() )
        
        if (line.startswith("#")): continue
        if("OK" == line[0:2] or "KO" == line[0:2]):
            break

    if ("KO" == line[0:2]):
        raise IOError()
    else:
        if output is not None:
            output.append( line.strip().split(" ") )


def reset_device(pic18_address, ser):

    # <STX><ADDR><CMD><LEN><DATA0...DATAN-1><CRC16><CRC16><ETX>
    # 0x00 Reset Device Command Id
    # 0x00 Lenght of Data Fields (in this case no data)

    cmdId = 0
    cmd =  "{:02X}".format(pic18_address) + "{:02X}".format(cmdId) + "00"

    #hex_data = cmd.decode("hex")
    crc = CRCCCITT().calculate( bytes.fromhex(cmd) )  
    crc_str = "{:04X}".format( crc )
    cmd = "0F" + cmd + crc_str + "04"
    
    print ("Write Reset Cmd:[" + cmd+ "]")

    try:
        try_io(lambda:  write_uart_line(ser, cmd))
    except IOError as e:
        print (str(e))
        return 0
    return 1

def start_measure(pic18_address, ser):

    # <STX><ADDR><CMD><LEN><DATA0...DATAN-1><CRC16><CRC16><ETX>
    # 0x01 Start Measure Command Id
    # 0x00 Lenght of Data Fields (in this case no data)

    cmdId = 1
    cmd =  "{:02X}".format(pic18_address) + "{:02X}".format(cmdId) + "00"

    #hex_data = cmd.decode("hex")
    crc = CRCCCITT().calculate( bytes.fromhex(cmd) )  
    crc_str = "{:04X}".format( crc )
    cmd = "0F" + cmd + crc_str + "04"
    
    print ("Write Start Measure Cmd:[" + cmd+ "]")

    try:
        try_io(lambda:  write_uart_line(ser, cmd, timeout=75))
    except IOError as e:
        print (str(e))
        return 0
    return 1

def read_measure(pic18_address, ser):

    # <STX><ADDR><CMD><LEN><DATA0...DATAN-1><CRC16><CRC16><ETX>
    # 0x02 Read Measure Command Id
    # 0x00 Lenght of Data Fields (in this case no data)

    cmdId = 2
    cmd =  "{:02X}".format(pic18_address) + "{:02X}".format(cmdId) + "00" 

    #hex_data = cmd.decode("hex")
    crc = CRCCCITT().calculate( bytes.fromhex(cmd) )  
    crc_str = "{:04X}".format( crc )
    cmd = "0F" + cmd + crc_str + "04"
    
    print ("Write Read Measure Cmd:[" + cmd+ "]")

    response = []   # empty array
    try:
        try_io(lambda:  write_uart_line(ser, cmd, 20, response))
    except Exception as e:
        print (str(e))
        return response
    
    #print("Dump:" + str(response))
    return response

def setup_database(con):
    try:
        cursor = con.cursor()
        
        #create 'measure' table
        #cursor.execute('''DROP TABLE measure''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS measure (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, device TEXT, value INTEGER, processed CHAR(1) DEFAULT 'N', timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)''')

        #create 'events' table
        #cursor.execute('''DROP TABLE events''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, device TEXT, event TEST, level VARCHAR(10), timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)''')
        
        #create 'crews' table
        #cursor.execute('''DROP TABLE crews''')
        #cursor.execute('''CREATE TABLE IF NOT EXISTS crews(id INTEGER, first_name TEXT, other_names TEXT, role TEXT, stage_name TEXT, biography TEXT, picture TEXT)''')
        
        #create 'configuration' table
        #cursor.execute('''DROP TABLE configuration''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS configuration(key TEXT, value TEXT)''')
    
    except Exception as e:
        raise e

def insert_measure(connection, value):
    try:
        cursor = connection.cursor()    
        cursor.execute("INSERT INTO measure (device,value) VALUES(?,?)", ("power-meter",value))
        connection.commit()
    except Exception as e:
        print(str(e))
        raise e

def insert_event(connection, event,level="DEBUG"):
    
    try:
        cursor = connection.cursor()    
        cursor.execute("INSERT INTO events (device,event,level) VALUES(?,?,?)", ("power-meter", event, level))
        connection.commit()
    except Exception as e:
        print(str(e))
        raise e


if __name__ == '__main__':
    
    
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))

    if len(sys.argv)> 1:
        filename = sys.argv[1]
    else:
        filename = "C:\\Dati\\workspace\\misuratore_consumi\\dist\\default\\production\\misuratore_consumi.production.hex"
    
    print("Open database\r\n")
    try:        
        connection = sqlite3.connect("../power_meter.db")
        setup_database(connection)
    except Exception as e:        
        print(str(e))
        quit()


    ser = None
    print("Open serial port to Pic18\r\n")
    insert_event(connection, "Open serial port to Pic18")
    try:
        ser = serial.Serial(timeout=10)
        ser.baudrate = 9600#115200
        #ser.port = 'COM3'
        ser.port = '/dev/ttyAMA0'
        ser.open()

        insert_event(connection, "Start Measure")
        start_measure(0x10,ser)
        response = read_measure(0x10, ser)
        insert_event(connection, "Read Measure " + str(response))
        
        if len(response)>1 and response[1].isdigit():
            insert_measure(connection,int(response[1]))

        ser.close()
    except Exception as e:
        insert_event(connection, str(e))        
        print(str(e))
    
    connection.close()

    print ("Done!")
