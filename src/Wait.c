#include <xc.h>         /* XC8 General Include File */

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */
#include "Wait.h"


/* Delay10KTCYx
 * Delay multiples of 10,000 Tcy
 * Passing 0 (zero) results in a delay of 2,560,000 cycles.
 */
/////////////////Fosc = 8////////////////////////////
// Fcy = Fosc/4  ->  Fcy = 8 Mhz/4 = 2 Mhz  ->
// -> Tcy = 1/2000000 = 0,0000005 sec = 0,5 us 


void WaitOneSec(){
    Delay10KTCYx(200);
}

void WaitMs(int j){
    for (int i = 0; i <j; i++){
        Delay1KTCYx(2);
    }
}


