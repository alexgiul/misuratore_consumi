/*
 * File:   Wait.h
 * Author: VALERIO
 *
 * Created on 15 giugno 2015, 15.56
 */

#ifndef WAIT_H
#define	WAIT_H

#ifndef plib_delays_header
#define plib_delays_header
    #include <plib/delays.h>
#endif

void WaitOneSec();
void WaitMs(int);

#endif	/* WAIT_H */
