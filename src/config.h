#ifndef CONFIG_H
#define	CONFIG_H

#include <stdint.h>

#define DEVICE_ID           0x10

#define LED             LATAbits.LATA5//PORTAbits.RA5
#define LED_TRIS        TRISAbits.RA5

/////////////////////////////////////////////////Registro per i local configuration DIP_SWITCH
typedef union {
    uint8_t val;
    struct {
        uint8_t ID1 : 1;
        uint8_t ID2 : 1;
        uint8_t ID3 : 1;
        uint8_t ID4 : 1;
        uint8_t ID5 : 1;
        uint8_t ID6 : 1;
        uint8_t ID7 : 1;
        uint8_t ID8 : 1;
    } flags;
} SYSTEM_ID;

////////////////////////////////////////////////////////////////////////

typedef union {
    uint8_t val;
    struct {
        uint8_t new_usart_data_flag : 1;
        uint8_t : 1;
        uint8_t enable_countdown : 1;        
        uint8_t cmd_successfully_executed : 1;
        uint8_t : 1;
        uint8_t : 1;
        uint8_t : 1;
        uint8_t : 1;            // MSB
    } bits;
} SYSTEM_STATUS;

#define USART_BUFFER    64

#define STX 0x0F
#define ETX 0x04


typedef union {
    uint8_t val;
    struct {
        uint8_t valid : 1;
        uint8_t invalid_stx : 1;
        uint8_t invalid_etx : 1;
        uint8_t invalid_fw_crc : 1;
        uint8_t invalid_pck_crc : 1;
        uint8_t invalid_device_id: 1;        
        uint8_t : 1;
        uint8_t : 1;            // MSB
    } bits;
} CMD_Status;

typedef union {

    uint8_t val[4+32+2+sizeof(CMD_Status)];

    struct {
        uint8_t addr;
        uint8_t cmd;
        uint8_t data_len;        
        uint8_t data[8];
        uint16_t crc;   // packet CRC

        CMD_Status status;
    };

} SYSTEM_CMD;

#endif	/* CONFIG_H */