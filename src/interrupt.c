/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#include <xc.h>         /* XC8 General Include File */
#include <usart.h>

#pragma warning disable 359

volatile unsigned char seconds =0;
volatile unsigned char rb0_events = 0;
volatile unsigned char measure_in_progress = 0;

/******************************************************************************/
/* Interrupt Routines                                                         */

/******************************************************************************/
void interrupt high_priority high_isr(void) {
    // Controllo che l'interrupt sia stato generato da RB0
    if (INTCONbits.INT0IF == 1) 
    {
        if(measure_in_progress) rb0_events++;
        
        INTCONbits.INT0IF = 0; // RIABILITO L'INTERRUPT RB0
    }
}

void interrupt low_priority low_isr(void) {
	if (INTCONbits.TMR0IF == 1 ) {
         TMR0H = 0xC2;
         TMR0L = 0xFA; //0xF7;
         INTCONbits.TMR0IF = 0;

        if(measure_in_progress) seconds++;
        if(seconds==60) {
             seconds = 0;
             measure_in_progress = 0;
         }
         /* Resetto il flag d'interrupt per permettere nuove interruzioni Timer 0 */
	}
}
