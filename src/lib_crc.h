
#ifndef CRC_H
#define	CRC_H

    // https://www.lammertbies.nl/comm/info/crc-calculation.html
    // CRC-CCITT (XModem)
    uint16_t gen_crc16(const uint8_t *data, uint16_t size);

#endif
