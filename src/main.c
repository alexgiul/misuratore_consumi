#include <xc.h>

#include <stdio.h>
#include <stdint.h>

#include "Wait.h"
#include "usart_interface.h"
#include "config.h"

#pragma warning disable 359

volatile static SYSTEM_ID system_id;
volatile SYSTEM_STATUS bs;

extern unsigned char seconds;
extern unsigned char measure_in_progress;
extern unsigned char rb0_events;
unsigned char buffer[24];

void process_cmd(SYSTEM_CMD*);
void display_cmd_errors(SYSTEM_CMD* cmd);

void ConfigureOscillator(void)
{
    //set it to use the 8MHz internal clock.
    OSCCONbits.IRCF2 = 1;
    OSCCONbits.IRCF1 = 1;
    OSCCONbits.IRCF0 = 0;
}

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

void InitApp()
{
    system_id.val = DEVICE_ID;

    ANSELC = 0; // ABILITO IL BUFFER DIGITALE PER LA PORTA C

    TRISCbits.TRISC7 = 1;   //USART RX
    TRISCbits.TRISC6 = 1;   //USART TX
    
    LED_TRIS = 0;
    LED = 0;

    //DisablePullups();
    INTCON2bits.RBPU=0;     //
    WPUB = 0;
//    WPUBbits.WPUB3 = 1; // enable pullup on PORTB.RB3 (SWITCH)

    //************************************************************
    // Abilito le interruzioni per funzionare ad alta priorita'
    //************************************************************
    //IOCBbits.IOCB4
    //INTCONbits.RBIE 	= 1;	// INTERRUPT RB4-RB7
    INTCONbits.INT0IE = 1; // INTERRUPT RB0 - ON
    INTCON2bits.RBIP = 1; // INTERRUPT RB0 HIGH PRIORITY
    INTCON2bits.INTEDG0 = 1; // 1 = Interrupt on rising edge,0 = Interrupt on falling edge
    //**********************************************************

    /* Initialize peripherals */

    //************************************************************
    // TIMER 0 - USATO PER GENERARE IL TEMPO
    //************************************************************
     T0CONbits.T08BIT 	= 0; 	// 1 = Timer0 is configured as an 8-bit timer/counter,0 = Timer0 is configured as a 16-bit timer/counter
     T0CONbits.T0SE 	= 0; 	// Increment on low-to-high transition on T0CKI pin
     T0CONbits.T0CS 	= 0; 	// 1 = RA4/TOCKI, 0 = Internal instruction cycle clock (CLKO)
     T0CONbits.PSA      = 0; 	// prescaler off
     T0CONbits.T0PS2	= 1;	// 1:128
     T0CONbits.T0PS1	= 1;
     T0CONbits.T0PS0	= 0;
     T0CONbits.TMR0ON 	= 0; 	//  TMR0 SPENTO
    
    // Abilito le interruzioni del Timer0
    INTCONbits.TMR0IE 	= 1;
    // Abilito le interruzioni del Timer0 come bassa priorita'
    INTCON2bits.TMR0IP = 0;
    //**********************************************************

    ///////////////////////////////////USART///////////////////////////////////
    USART_Interface_init();

    //************************************************************
    // Abilito le interruzioni
    //************************************************************
    // Abilito modalita interruzione a due livelli alta e bassa
    RCONbits.IPEN = 1;
    // Abilito gli interrupt ad alta priorita
    INTCONbits.GIEH = 1;
    // Abilito gli interrupt a bassa priorita
    INTCONbits.GIEL = 1;
}


/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/
void start_analisys(void)
{
    LED = 1;    // analisys in progress
    rb0_events = 0;
    USART_PutString("# Start analisys...\r\n");
    measure_in_progress = 1;
    TMR0H = 0xC2;
    TMR0L = 0xF7;
    T0CONbits.TMR0ON 	= 1; 	//  TMR0 ACCESO
    
    while(measure_in_progress) {
        //sprintf(buffer,"# %02d\r",seconds);
        //USART_PutString(buffer);
        //WaitOneSec();
        Nop();
        CLRWDT();
    }    
    T0CONbits.TMR0ON 	= 0; 	//  TMR0 SPENTO
    //USART_PutString("\r\n");
    LED = 0;    // analisys done    
    USART_PutString("# Analisys done!\r\n");
}

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/
void main(void) {

    volatile SYSTEM_CMD cmd;
    /* Configure the oscillator for the device */
    ConfigureOscillator();
    
    /* Initialize I/O and Peripherals for application */
    InitApp();

    WaitOneSec();
    
    USART_PutString("\r\nPOWER METER " __DATE__ " " __TIME__ "\r\n");
    
    while (1) {
        USART_Interface_receive_command(&bs);
        USART_Interface_process_buffer(&bs, &cmd, &system_id);

        display_cmd_errors(&cmd);
        process_cmd(&cmd);

        CLRWDT();
    }

}

void display_cmd_errors(SYSTEM_CMD* cmd) {

    if(1 == cmd->status.bits.valid) return;
    
    if(cmd->status.bits.invalid_etx) USART_PutString("KO Invalid ETX\r\n");
    if(cmd->status.bits.invalid_fw_crc) USART_PutString("KO Invalid FW CRC\r\n");
    if(cmd->status.bits.invalid_pck_crc) USART_PutString("KO Invalid PCK CRC\r\n");
    if(cmd->status.bits.invalid_stx) USART_PutString("KO Invalid STX\r\n");
    
    /* There is an error, reset flag of current command and skip processing */
    cmd->status.val = 0;
}


#define RESET_DEVICE   0x00
#define START_MEASURE   0x01
#define READ_MEASURE   0x02

void process_cmd(SYSTEM_CMD* cmd) {
    
    /* skip invalid commands */
    if (0 == cmd->status.bits.valid) return;
    /* reset command status */
    bs.bits.cmd_successfully_executed = 0;

    switch (cmd->cmd) {
        case RESET_DEVICE:
            USART_PutString("OK Reset Pic in 3 sec\r\n");
            WaitOneSec();
            WaitOneSec();
            WaitOneSec();
            Reset();
            break;

        case START_MEASURE:
            start_analisys();
            bs.bits.cmd_successfully_executed = 1;
            USART_PutString("OK Start Measure\r\n");
            break;
                
        case READ_MEASURE:
            sprintf(buffer,"OK %02d Read Measure\r\n",rb0_events);
            USART_PutString(buffer);
            bs.bits.cmd_successfully_executed = 1;
            break;

        default:
            /* unrecognized character */
            break;
    }
    cmd->status.bits.valid = 0;
}
