#include "usart_interface.h"
#include "lib_crc.h"
#include "utils.h"

#include <usart.h>
#include <string.h> // memeset

#include <plib/usart.h>

#pragma warning disable 359
#pragma warning disable 752 // conversion to shorter data type

//Local Variables
volatile uint8_t usartBufferCount = 0;
volatile uint8_t usartBuffer[USART_BUFFER];

/////////////////////ESUART 1/////////////////////

void USART_Interface_init(void)
{
    // Quarzo 8MHz
    // Formato 8 bit
    // 1 bit di stop
    // Baud rate 9600bit/s
    // Interruzioni disattive
    // RS232 to RASP

    Open1USART(USART_TX_INT_OFF &
        USART_RX_INT_OFF &
        USART_ASYNCH_MODE &
        USART_EIGHT_BIT &
        //USART_SINGLE_RX &
        USART_CONT_RX &
        USART_BRGH_LOW,
        12);

}
/*********************************************************************
* Function:         void USART_PutString(char* str)
*
* PreCondition:     none
*
* Input:            str - string that needs to be printed
*
* Output:           none
*
* Side Effects:	    str is printed to the USART
*
* Overview:         This function will print the inputed ROM string
*
* Note:             Do not power down the microcontroller until
*                   the transmission is complete or the last
*                   transmission of the string can be corrupted.
********************************************************************/
void USART_PutString(char* str)
{
 do
  {
     if(*str == 0) break;
     // Transmit a byte
     while(Busy1USART());
     putc1USART(*str);
  } while( *str++ );
}


/*********************************************************************
* Function:         unsigned char uart_recv_uchar()
*
* PreCondition:     none
*
* Input:            none
*
* Output:           0 or received char
*
* Side Effects:	    none
*
* Overview:         This function will receive a char from USART 1
*
* Note:             This function is not blocking.
*
*
********************************************************************/
unsigned char uart_recv_uchar() {
    if (RCSTA1bits.OERR) {
        /* overrun error, reset CREN */
        RCSTA1bits.CREN = 0;
        RCSTA1bits.CREN = 1;
    }

    char data;
    if( PIR1bits.RC1IF)
        data = RCREG1;
    else
        data = 0;

    return data;
}


void USART_Interface_process_buffer(SYSTEM_STATUS* bs, SYSTEM_CMD* cmd, SYSTEM_ID* system_id) {

    if (0 == bs->bits.new_usart_data_flag) return;

    // Reset Command internal flags
    cmd->status.val = 0;
#ifdef DEBUG
    USART_PutString("# Process Usart Buffer Start\r\n");
#endif
    // reset flag of processed packet
    bs->bits.new_usart_data_flag = 0;

    /*
     * Cmd Structure
     * <STX><ADDR><CMD><LEN><DATA0...DATAN-1><CRC16><CRC16><ETX>
     * */
    // check 1
    uint8_t stx = hexChar2num(usartBuffer[0])*16 + hexChar2num(usartBuffer[1]);
    if (STX != stx) {
        cmd->status.bits.invalid_stx = 1;
        return;
    }

    cmd->addr = hexChar2num(usartBuffer[2])*16 + hexChar2num(usartBuffer[3]);
    cmd->cmd = hexChar2num(usartBuffer[4])*16 + hexChar2num(usartBuffer[5]);
    cmd->data_len = hexChar2num(usartBuffer[6])*16 + hexChar2num(usartBuffer[7]);
    
    memset(cmd->data, 0, sizeof (cmd->data));

    uint8_t i = 0;
    uint8_t u = 0;
    while (cmd->data_len != 0 && i < cmd->data_len) {
        cmd->data[i] = hexChar2num(usartBuffer[8 + u])*16 + hexChar2num(usartBuffer[9 + u]);
        u += 2;
        i++;
    }
    // check 2 - check internal CRC
    short check_crc = 0;

    for (i = 0; i < cmd->data_len; i++)
        check_crc += cmd->data[i];

    /* a valid Hex Intel String contains a checksum */
    if ( (cmd->data_len != 0) && (check_crc & 0xFF) ) { //wrong FW CRC
        cmd->status.bits.invalid_fw_crc = 1;
        return;
    }

    // check 3 : verifica che il CRC a 16 bit di tutto il pacchetto sia corretto
    uint16_t packet_crc = gen_crc16(cmd->val, 3+cmd->data_len );

    uint8_t crc_hb = hexChar2num(usartBuffer[8 + u])*16 + hexChar2num(usartBuffer[9 + u]); // HB CRC
    u += 2;
    uint8_t crc_lb = hexChar2num(usartBuffer[8 + u])*16 + hexChar2num(usartBuffer[9 + u]); // LB CRC
    cmd->crc = crc_hb * 256 + crc_lb; // CRC of whole packet
    if (packet_crc != cmd->crc) { //wrong PCK CRC
        cmd->status.bits.invalid_pck_crc = 1;
        return;
    }

    // check 4 - Check end of trasmission
    u += 2;
    uint8_t etx = hexChar2num(usartBuffer[8 + u])*16 + hexChar2num(usartBuffer[9 + u]);
    if (ETX != etx) {
        cmd->status.bits.invalid_etx = 1;
        return;
    }

    // check 5 : il comando e' per questo device???
    if (system_id->val != cmd->addr) {
        cmd->status.bits.invalid_device_id = 1;
        return;
    }
    // Ok comando valido e per questo device
    cmd->status.bits.valid = 1;
    // ho ricevuto un comando, esco dalla modalita' COUNT-DOWN
    bs->bits.enable_countdown = 0;

    memset(usartBuffer,0, USART_BUFFER);
#ifdef DEBUG
    USART_PutString("# Process Usart Buffer Complete Succefully\r\n");
#endif
}

void USART_Interface_receive_command(SYSTEM_STATUS* bs) {
    uint8_t receved_char = uart_recv_uchar();
    uint8_t len;
    asm("nop");
    if (0 == receved_char) return;

    if (usartBufferCount >= USART_BUFFER) usartBufferCount = 0;
    //assert( usartBufferCount < USART_BUFFER);
    usartBuffer[usartBufferCount] = receved_char;

    if (usartBuffer[usartBufferCount] == '\n' || usartBuffer[usartBufferCount] == '\r') {
        bs->bits.new_usart_data_flag = 1;
        len = usartBufferCount; // effective len of received buffer
        usartBufferCount++;
        if (usartBufferCount < USART_BUFFER) usartBuffer[usartBufferCount] = 0;
        usartBufferCount = 0;
    } else
        usartBufferCount++;

    if (1 == bs->bits.new_usart_data_flag && len == 0)
        bs->bits.new_usart_data_flag = 0;
}
