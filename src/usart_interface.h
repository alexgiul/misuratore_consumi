/*
 * File:   myusart.h
 * Author: VALERIO
 *
 * Created on 7 luglio 2015, 12.57
 */

#ifndef USART_INTERFACE_H
#define	USART_INTERFACE_H

#include "config.h"

    void USART_PutString(char*);

    void USART_Interface_init(void);
    void USART_Interface_process_buffer(SYSTEM_STATUS* bs, SYSTEM_CMD* cmd, SYSTEM_ID* system_id);
    void USART_Interface_receive_command(SYSTEM_STATUS* bs);

#endif	/* USART_INTERFACE_H */

