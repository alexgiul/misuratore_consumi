#include "utils.h"

#pragma warning disable 752

const unsigned char CharacterArray[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

void num2hex(unsigned char value, char* hexStr) {
    unsigned char tmp = value;
    hexStr[1] = CharacterArray[tmp & 0x0F];
    tmp = tmp >> 4;
    hexStr[0] = CharacterArray[tmp & 0x0F];
}

unsigned char hexChar2num(unsigned char value) {
    unsigned char tmp = 0;
    tmp = (value <= '9') ? (value - '0') : (value - 'A' + 10); // edited earlier error (forgot to add '+10')
    return tmp;
}