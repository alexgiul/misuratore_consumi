/* 
 * File:   utils.h
 * Author: giuli370
 *
 * Created on 15 ottobre 2016, 15.55
 */

#ifndef UTILS_H
#define	UTILS_H

#ifdef	__cplusplus
extern "C" {
#endif

    void num2hex(unsigned char value, char* hexStr);
    unsigned char hexChar2num(unsigned char value);


#ifdef	__cplusplus
}
#endif

#endif	/* UTILS_H */

